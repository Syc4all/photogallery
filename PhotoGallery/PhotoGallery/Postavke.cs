﻿using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace PhotoGallery
{
    class Postavke
    {
        private const string PutanjaPostavki = "settings.cfg";
        public List<Galerija> galerije;

        public Postavke()
        {
            galerije = new List<Galerija>();
        }

        public void UcitajPostavke()
        {
            if(!File.Exists(PutanjaPostavki))
            {
                KreirajPostavke();
                return;
            }

            StreamReader reader = new StreamReader(PutanjaPostavki);

            while(!reader.EndOfStream)
            {
                Galerija galerija = new Galerija();
                galerija.imeGalerije = reader.ReadLine();
                galerija.lokacijaGalerije = reader.ReadLine();
                galerija.UcitajSlike();
                galerije.Add(galerija);
            }
            reader.Close();
        }

        public void KreirajPostavke()
        {
            File.Create(PutanjaPostavki).Close();
        }

        public void DodajGaleriju(string putanja)
        {
            //Ime galerije je jednaka putanji minus 4 zadnja znaka ".gly"
            File.Create(putanja).Close();
            Galerija galerija = new Galerija();
            galerija.lokacijaGalerije = putanja;
            galerija.imeGalerije = Path.GetFileNameWithoutExtension(putanja);
            galerije.Add(galerija);
            DodajGalerijuUPostavke(galerija);
        }

        private void DodajGalerijuUPostavke(Galerija galerija)
        {
            StreamWriter writer = new StreamWriter(PutanjaPostavki, true);
            writer.WriteLine(galerija.imeGalerije);
            writer.WriteLine(galerija.lokacijaGalerije);
            writer.Close();
        }
    }
}
