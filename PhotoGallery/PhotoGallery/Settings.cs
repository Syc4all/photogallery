﻿using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace PhotoGallery
{
    class Settings
    {
        private const string settingsLocation = "settings.cfg";
        public List<Gallery> galleries;

        public Settings()
        {
            galleries = new List<Gallery>();
        }

        public void loadSettings()
        {
            if(!File.Exists(settingsLocation))
            {
                createSettings();
                return;
            }

            StreamReader reader = new StreamReader(settingsLocation);

            while(!reader.EndOfStream)
            {
                Gallery gallery = new Gallery();
                gallery.galleryName = reader.ReadLine();
                gallery.galleryLocation = reader.ReadLine();
                gallery.loadPictures();
                galleries.Add(gallery);
            }
            reader.Close();
        }

        public void createSettings()
        {
            File.Create(settingsLocation).Close();
        }

        public void addGallery(string location)
        {
            //Ime galerije je jednaka putanji minus 4 zadnja znaka ".gly"
            File.Create(location).Close();
            Gallery gallery = new Gallery();
            gallery.galleryLocation = location;
            gallery.galleryName = Path.GetFileNameWithoutExtension(location);
            galleries.Add(gallery);
            addGalleryToSettings(gallery);
        }

        private void addGalleryToSettings(Gallery gallery)
        {
            StreamWriter writer = new StreamWriter(settingsLocation, true);
            writer.WriteLine(gallery.galleryName);
            writer.WriteLine(gallery.galleryLocation);
            writer.Close();
        }
    }
}
