﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoGallery
{
    public class Gallery
    {
        public string galleryName { get; set; }
        public string galleryLocation { get; set; }

        public List<string> pictures { get; set; }

        public Gallery()
        {
            pictures = new List<string>();
        }

        public void loadPictures()
        {
            if(!File.Exists(galleryLocation))
            {
                return;
            }

            StreamReader reader = new StreamReader(galleryLocation);
            while(!reader.EndOfStream)
            {
                pictures.Add(reader.ReadLine());
            }
        }

        public void addPictures(List<string> newPictures)
        {
            foreach(string picture in newPictures)
            {
                pictures.Add(picture);
            }
            saveChanges();
        }

        private void saveChanges()
        {
            StreamWriter writer = new StreamWriter(galleryLocation);
            foreach(string picture in pictures)
            {
                writer.WriteLine(picture);
            }
            writer.Close();
        }
    
    }
}


