﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoGallery
{
    class GalleryDisplay
    {
        public Gallery gallery { get; set; }
        public PictureBox thumbnail { get; set; }
        public Label galleryName { get; set; }
        Form form;

        public int positionX;
        public int positionY;

        const int width = 100;
        const int height = 100;

        public GalleryDisplay(int posX, int posY, Gallery gal, Form form)
        {
            positionX = posX;
            positionY = posY;
            gallery = gal;
            this.form = form;
            
            createPicture();
            createName();
        }

        private void createPicture()
        {
            thumbnail = new PictureBox();
            form.Controls.Add(thumbnail);
            thumbnail.Location = new System.Drawing.Point(positionX, positionY);
            thumbnail.Size = new System.Drawing.Size(width, height);
            thumbnail.BackColor = System.Drawing.Color.Gray;
            thumbnail.SizeMode = PictureBoxSizeMode.StretchImage;
            thumbnail.Click += galleryPicture_Click;
            if(gallery.pictures.Count > 0)
            {
                thumbnail.ImageLocation = gallery.pictures[0];
            }
        }

        private void galleryPicture_Click(object sender, EventArgs e)
        {
            Form2 galleryForm = new Form2(gallery);
            galleryForm.ShowDialog();
            if(gallery.pictures.Count > 0)
            {
                thumbnail.ImageLocation = gallery.pictures[0];
            }
        }

        private void createName()
        {
            int y = positionY + thumbnail.Height + 20;
            galleryName = new Label();
            form.Controls.Add(galleryName);

            galleryName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            galleryName.Location = new System.Drawing.Point(positionX, y);
            galleryName.Width = width;
            galleryName.Text = gallery.galleryName;
        }

        public void Delete()
        {
            form.Controls.Remove(thumbnail);
            form.Controls.Remove(galleryName);
        }
    }
}
