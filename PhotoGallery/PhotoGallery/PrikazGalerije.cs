﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoGallery
{
    class PrikazGalerije
    {
        public Galerija galerija { get; set; }
        public PictureBox slikaGalerije { get; set; }
        public Label imeGalerije { get; set; }
        Form forma;

        public int pozicijaX;
        public int pozicijaY;

        const int sirina = 100;
        const int visina = 100;

        public PrikazGalerije(int pozX, int pozY, Galerija gal, Form form)
        {
            pozicijaX = pozX;
            pozicijaY = pozY;
            galerija = gal;
            forma = form;
            
            KreirajSliku();
            KreirajIme();
        }

        private void KreirajSliku()
        {
            slikaGalerije = new PictureBox();
            forma.Controls.Add(slikaGalerije);
            slikaGalerije.Location = new System.Drawing.Point(pozicijaX, pozicijaY);
            slikaGalerije.Size = new System.Drawing.Size(sirina, visina);
            slikaGalerije.BackColor = System.Drawing.Color.Gray;
            slikaGalerije.SizeMode = PictureBoxSizeMode.StretchImage;
            slikaGalerije.Click += SlikaGalerije_Click;
            if(galerija.slike.Count > 0)
            {
                slikaGalerije.ImageLocation = galerija.slike[0];
            }
        }

        private void SlikaGalerije_Click(object sender, EventArgs e)
        {
            Form2 galerijaForm = new Form2(galerija);
            galerijaForm.ShowDialog();
            if(galerija.slike.Count > 0)
            {
                slikaGalerije.ImageLocation = galerija.slike[0];
            }
        }

        private void KreirajIme()
        {
            int y = pozicijaY + slikaGalerije.Height + 20;
            imeGalerije = new Label();
            forma.Controls.Add(imeGalerije);

            imeGalerije.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            imeGalerije.Location = new System.Drawing.Point(pozicijaX, y);
            imeGalerije.Width = sirina;
            imeGalerije.Text = galerija.imeGalerije;
        }

        public void Obriši()
        {
            forma.Controls.Remove(slikaGalerije);
            forma.Controls.Remove(imeGalerije);
        }
    }
}
