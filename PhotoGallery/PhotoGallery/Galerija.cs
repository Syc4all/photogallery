﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoGallery
{
    public class Galerija
    {
        public string imeGalerije { get; set; }
        public string lokacijaGalerije { get; set; }

        public List<string> slike { get; set; }

        public Galerija()
        {
            slike = new List<string>();
        }

        public void UcitajSlike()
        {
            if(!File.Exists(lokacijaGalerije))
            {
                return;
            }

            StreamReader reader = new StreamReader(lokacijaGalerije);
            while(!reader.EndOfStream)
            {
                slike.Add(reader.ReadLine());
            }
        }

        public void DodajSlike(List<string> noveSlike)
        {
            foreach(string slika in noveSlike)
            {
                slike.Add(slika);
            }
            SpremiPromjene();
        }

        private void SpremiPromjene()
        {
            StreamWriter writer = new StreamWriter(lokacijaGalerije);
            foreach(string slika in slike)
            {
                writer.WriteLine(slika);
            }
            writer.Close();
        }
    
    }
}


