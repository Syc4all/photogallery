﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace PhotoGallery
{
    public partial class Form2 : Form
    {
        private Gallery odabranaGalerija;

        List<PictureBox> slike;

        public Form2()
        {
            InitializeComponent();
        }

        public Form2(Gallery gal)
        {
            InitializeComponent();
            odabranaGalerija = gal;
        }

        public void ucitavanjeSlika()
        {
            if(slike.Count > 0)
            {
                foreach(PictureBox pb in slike)
                {
                    Controls.Remove(pb);
                }

                slike.Clear();
            }

            int x = 10;
            int y = 10;
            int offset = 110;
            int size = 100;

            int red = 0;
            int stupac = 0;

            foreach(string putanja in odabranaGalerija.pictures)
            {
                PictureBox pb = new PictureBox();
                pb.Size = new Size(size, size);
                pb.Location = new Point(x + offset * stupac, y + offset * red);
                pb.ImageLocation = putanja;
                pb.SizeMode = PictureBoxSizeMode.StretchImage;
                Controls.Add(pb);
                slike.Add(pb);

                stupac++;
                if(stupac >= 5)
                {
                    stupac = 0;
                    red++;
                }
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            slike = new List<PictureBox>();
            Text = odabranaGalerija.galleryName;
            ucitavanjeSlika();
        }

        private void addPicturesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dodajSlikuDialog.ShowDialog();
            List<string> noveSlike = new List<string>();
            foreach(string slika in dodajSlikuDialog.FileNames)
            {
                noveSlike.Add(slika);
            }
            odabranaGalerija.addPictures(noveSlike);
            ucitavanjeSlika();
        }
    }
}
