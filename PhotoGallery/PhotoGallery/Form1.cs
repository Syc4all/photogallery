﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace PhotoGallery
{
    public partial class PhotoGallery : Form
    {
        Settings settings;
        List<GalleryDisplay> galleryDisplays;

        public PhotoGallery()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            galleryDisplays = new List<GalleryDisplay>();
            settings = new Settings();
            settings.loadSettings();
            generateDisplays();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog newGallery = new SaveFileDialog();
            newGallery.Filter = "Gly|*.gly";
            newGallery.Title = "Save New Gallery";
            if(newGallery.ShowDialog() == DialogResult.OK)
            {
                settings.addGallery(newGallery.FileName);
                generateDisplays();
            } 
        }

        private void addImageToGalleryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog newPicture = new OpenFileDialog();
            newPicture.Filter = "PNG|*.png";
            newPicture.Title = " Add New Picture To Gallery";
            
        }

        private void generateDisplays()
        {
            if (galleryDisplays.Count > 0)
            {
                foreach (GalleryDisplay p in galleryDisplays)
                {
                    p.Delete();
                }
                galleryDisplays.Clear();
            }
            int x = 10;
            int y = 10;
            int xOffset = 110;
            int yOffset = 200;
            int inRow = 0;
            int row = 0;
            foreach (Gallery gal in settings.galleries)
            {
                GalleryDisplay display = new GalleryDisplay(x + xOffset * inRow, y + yOffset * row, gal, this);
                galleryDisplays.Add(display);
                inRow++;
                if (inRow >= 5)
                {
                    inRow = 0;
                    row++;
                }
            }
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
